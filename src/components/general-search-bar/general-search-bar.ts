import {Component} from '@angular/core';
import {Events} from "ionic-angular";

@Component({
    selector: 'general-search-bar',
    templateUrl: 'general-search-bar.html'
})
export class GeneralSearchBarComponent {

    public generalSearchInput: string;

    /**
     * Constructor
     *
     * @param {Events} events
     */
    constructor(private events: Events) {
    }

    /**
     *  Publish an event i search cases
     */
    generalSearch() {
        if (this.generalSearchInput) {
            this.events.publish('generalSearchBar', this.generalSearchInput);
        }
    }

}
