import { NgModule } from '@angular/core';
import { GeneralSearchBarComponent } from './general-search-bar/general-search-bar';
import { FiltersComponent } from './filters/filters';

@NgModule({
	declarations: [GeneralSearchBarComponent, FiltersComponent],
	imports: [],
	exports: [GeneralSearchBarComponent, FiltersComponent]
})
export class ComponentsModule {}
