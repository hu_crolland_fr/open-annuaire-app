import {Component} from '@angular/core';
import {categories, depet, nj, rpens, tcas, tefens} from "../../data/lists";
import {Events} from "ionic-angular";
import {Filter} from "../../models/filter";

@Component({
    selector: 'filters',
    templateUrl: 'filters.html'
})
export class FiltersComponent {

    // Inputs
    ape: string;
    city: string;
    creationYear: string;
    region: Array<string>;
    turnover: Array<string>;
    workForce: Array<string>;
    legalForm: Array<string>;
    departement: Array<string>;
    category: Array<string>;

    // Select/option elements
    turnoverList: any;
    workForceList: any;
    legalFormList: any;
    departementList: any;
    categoryList: any;
    regionsList: any;

    selecteApe = [];
    selectedCities = [];
    selectedCreationYears = [];
    selectedCategories = [];

    settedFilters: Filter;

    /**
     * Controller
     *
     * @param {Events} events
     */
    constructor(private events: Events) {
        this.regionsList = rpens.getCollection();
        this.departementList = depet.getCollection();
        this.categoryList = categories.getCollection();
        this.workForceList = tefens.getCollection();
        this.turnoverList = tcas.getCollection();
        this.legalFormList = nj.getCollection();
    }

    /**
     * Search by filters.
     *
     * @param value
     * @param {any[]} selectedList
     */
    searchByFilters(value: any = "", selectedList = []) {
        const filter = new Filter();

        if (value) {
            this.removeFromList(value, selectedList);
        }

        if (this.ape) filter.ape = this.setMultipleValueForInput(this.ape, this.selecteApe);
        this.ape = "";
        if (this.city) filter.city = this.setMultipleValueForInput(this.city, this.selectedCities);
        this.city = "";
        if (this.creationYear) filter.creationYear = this.setMultipleValueForInput(this.creationYear, this.selectedCreationYears);
        if (this.category) filter.category = this.category;
        if (this.region) filter.region = this.region;
        if (this.turnover) filter.turnover = this.turnover;
        if (this.workForce) filter.workForce = this.workForce;
        if (this.legalForm) filter.legalForm = this.legalForm;
        if (this.departement) filter.departement = this.departement;
        this.settedFilters = filter;
        this.publishEvents('filters', filter);
    }

    /**
     * Set multiple value for an input.
     *
     * @param {string} value
     * @param {any[]} selectedArray
     * @returns {any[] | undefined}
     */
    private setMultipleValueForInput(value: string, selectedArray = []) {
        selectedArray.push(value);
        return selectedArray;
    }

    /**
     * If na input has multiple reset this.
     *
     * @param value
     * @param {any[]} selectedList
     */
    removeFromList(value: any, selectedList = []) {
        const valueIndex = selectedList.indexOf(value);
        selectedList.splice(valueIndex, 1);
    }

    /**
     * Public events.
     *
     * @param {string} topic
     * @param data
     */
    publishEvents(topic: string, data: any) {
        this.events.publish(topic, data);
    }

    /**
     * Clear all filters.
     */
    clearAllFilters() {
        this.selecteApe = [];
        this.selectedCities = [];
        this.selectedCreationYears = [];
        this.selectedCategories = [];

        this.region = [];
        this.turnover = [];
        this.workForce = [];
        this.legalForm = [];
        this.departement = [];
        this.category = [];

        this.publishEvents('filters', new Filter());
    }
}
