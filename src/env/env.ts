export const env = {
    apiUrl: "https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene&lang=fr",
    mapIframeUrl: "https://public.opendatasoft.com/explore/embed/dataset/sirene/map/?basemap=mapbox.streets",
    exportUrl: "https://public.opendatasoft.com/explore/dataset/sirene/download/",
};

