import {Component, OnInit} from '@angular/core';
import {Events, NavController, Platform, AlertController} from 'ionic-angular';
import {File} from '@ionic-native/file';
import {CompanyRepositoryProvider} from "../../providers/company-repository/company-repository";
import {FileTransfer, FileTransferObject} from "@ionic-native/file-transfer";
import {ExportServiceProvider} from "../../providers/export-service/export-service";

declare let cordova;

@Component({
    selector: 'page-export',
    templateUrl: 'export.html',
    providers: [FileTransfer, FileTransferObject, File]
})
export class ExportPage implements OnInit {

    private storageDirectory: string;

    /**
     * Constructor
     *
     * @param {NavController} navCtrl
     * @param {FileTransfer} transfer
     * @param {Platform} platfrom
     * @param {Events} events
     * @param {CompanyRepositoryProvider} companyRepository
     * @param {AlertController} alertCtrl
     * @param {ExportServiceProvider} exportService
     */
    constructor(public navCtrl: NavController,
                private transfer: FileTransfer,
                private platfrom: Platform,
                private events: Events,
                private companyRepository: CompanyRepositoryProvider,
                private alertCtrl: AlertController,
                private exportService: ExportServiceProvider) {

        if (this.platfrom.is('ios')) {
            this.storageDirectory = cordova.file.documentsDirectory;
        }

        if (this.platfrom.is('android')) {
            this.storageDirectory = cordova.file.externalDataDirectory;
        }
    }

    /**
     * loadDatas() to get params, if there is
     */
    ngOnInit() {
        this.companyRepository.loadDatas();
    }

    /**
     * Download the file
     *
     * @param {string} format
     */
    getTheFileIn(format: string) {

        const fileTransfer: FileTransferObject = this.transfer.create();

        fileTransfer.download(
            this.exportService.getUrl(format),
            this.exportService.getStorageDir(this.storageDirectory, format)
        ).then((entry) => {

            const successAlert = this.alertCtrl.create({
                title: `Succès`,
                subTitle: "Téléchargement effectué avec succès",
                buttons: ['Ok']
            });

            successAlert.present();

        }, (error) => {

            const failureAlert = this.alertCtrl.create({
                title: `Le téléchargement a échoué`,
                subTitle: `Erreur lors du téléchargement. Error code: ${error.code}`,
                buttons: ['Ok']
            });

            failureAlert.present();

        });
    }
}
