import {Injectable} from '@angular/core';
import {Events} from "ionic-angular";
import {env} from "../../env/env";

@Injectable()
export class ExportServiceProvider {

    private apiParams: string;
    private apiUrl: string = env.exportUrl;

    /**
     * Constructor
     * @param {Events} events
     */
    constructor(private events: Events) {
        this.events.subscribe('apiParams', (data) => {
            this.apiParams = data;
        })
    }

    /**
     * Generate url to export the file.
     *
     * @param {string} exportFormat
     * @returns {string}
     */
    getUrl(exportFormat: string) {
        let url = this.apiUrl;
        url += "?format=" + this.getFormat(exportFormat) + this.checkParams() + "&rows=" + this.getNumRows() + "&use_labels_for_header=true";
        return url;
    }

    /**
     * Generate the file name.
     *
     * @param {string} storageDirectory
     * @param {string} fileFormat
     * @returns {string}
     */
    getStorageDir(storageDirectory: string, fileFormat: string) {
        let dir = storageDirectory + 'companies.' + this.getFormat(fileFormat);
        return dir;
    }

    /**
     * Set the file format.
     *
     * @param {string} format
     * @returns {string}
     */
    private getFormat(format: string) {
        let exportFormat = "";

        switch (format) {
            case 'csv':
                exportFormat = "csv";
                break;
            case 'xls':
                exportFormat = "xls";
                break;
            case 'json':
                exportFormat = "json";
                break;
            default:
                exportFormat = "json";
                break;
        }

        return exportFormat;
    }

    /**
     * Set the url request.
     *
     * @returns {string}
     */
    private checkParams() {
        return (this.apiParams) ? "&q=" + this.apiParams : "";
    }

    /**
     * Set result rows number.
     *
     * @param {string} numRows
     * @returns {string}
     */
    private getNumRows(numRows: string = null) {
        return (numRows) ? numRows : "100";
    }

}
