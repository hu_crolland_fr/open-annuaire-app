import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs/Observable";
import {Http} from "@angular/http";
import {Events} from "ionic-angular";
import {Company} from "../../models/company";
import {Filter} from "../../models/filter";
import {env} from "../../env/env";

@Injectable()
export class CompanyRepositoryProvider {

    private apiUrl: string = env.apiUrl;
    private mapIframeUrl: string = env.mapIframeUrl;
    private apiParams: string;
    private companies: Company[] = [];
    private scrollNumber;

    /**
     * Constructor
     *
     * @param {Http} http
     * @param {Events} events
     */
    constructor(private http: Http, private events: Events) {

        this.events.subscribe('filters', (data) => {
            this.searchWithFilters(data);
        });

        this.events.subscribe('scrolledPage', (data) => {
            this.scrollNumber = data;
        });
    }

    /**
     * Load companies
     */
    loadDatas() {
        this.events.publish('startLoading', true);
        this.events.publish('apiParams', this.apiParams);
        this.events.publish('mapUrlUpdate', this.getMapIframeUrl());

        this.callApi().subscribe(
            data => {
                this.companies = data['records'].map(company => new Company(company));
                this.events.publish('companies', this.companies);
                this.events.publish('searchResultLength', this.companies.length);
                this.events.publish('endLoading', true);
            });
    }

    /**
     * Call api
     *
     * @returns {Observable<any>}
     */
    private callApi() {
        return this.http.get(this.getApiUrl())
            .map(this.extractData)
            .do(this.logResponse)
            .catch(this.catchError);
    }

    /**
     * Catch errors
     *
     * @param {Response | any} error
     * @returns {ErrorObservable}
     */
    private catchError(error: Response | any) {
        console.log(error);
        return Observable.throw(error.json() || "Server error");
    }

    /**
     * Log responses
     *
     * @param {Response} res
     */
    private logResponse(res: Response) {
        return console.log(res);
    }

    /**
     * Extract datas
     *
     * @param {Response | any} res
     * @returns {Promise<any>}
     */
    private extractData(res: Response | any) {
        return res.json();
    }

    /**
     * Generate api url parameters
     *
     * @param {Filter} filter
     */
    setParams(filter: Filter = new Filter()) {
        this.resetApiParams();

        if (filter.generalSearch) this.apiParams = filter.generalSearch;

        if (filter.ape) {
            this.apiParams += this.createParamByProperty("(apet700:", filter.ape);
        }

        if (filter.category) {
            if (filter.category.length) this.apiParams += this.createParamByProperty("(categorie:", filter.category);
        }

        if (filter.departement) {
            if (filter.departement.length) this.apiParams += this.createParamByProperty("(depet:", filter.departement);
        }

        if (filter.creationYear) {
            if (filter.creationYear.length) this.apiParams += this.createParamByProperty("(dapen:", filter.creationYear);
        }

        if (filter.turnover) {
            if (filter.turnover.length) this.apiParams += this.createParamByProperty("(tca:", filter.turnover);
        }

        if (filter.workForce) {
            if (filter.workForce.length) this.apiParams += this.createParamByProperty("(tefen:", filter.workForce);
        }

        if (filter.legalForm) {
            if (filter.legalForm.length) this.apiParams += this.createParamByProperty("(nj:", filter.legalForm);
        }

        if (filter.region) {
            if (filter.region.length) this.apiParams += this.createParamByProperty("(rpen:", filter.region);
        }

    }

    /**
     * Create params with the properties.
     *
     * @param {string} parameter
     * @param {Filter} property
     * @returns {string}
     */
    createParamByProperty(parameter: string, property: Filter) {
        return this.paramStart() + parameter + this.setMultipleParams(property);
    }

    /**
     * Create and return tha api url with all parameters.
     *
     * @returns {string}
     */
    private getApiUrl() {

        let url = this.apiUrl;
        url += "&rows=" + this.dataRowsCalculator(this.scrollNumber);
        if (this.apiParams) url += "&q=" + this.apiParams;
        return url;

    }

    /**
     * Create and return the url for the map with all parameters.
     *
     * @returns {string}
     */
    public getMapIframeUrl() {

        let mapIframeUrl = this.mapIframeUrl;
        if (this.apiParams) mapIframeUrl += "&q=" + this.apiParams;
        console.log("event passed in company repo");
        return mapIframeUrl;
    }

    /**
     * Set multiple parameters.
     *
     * @param elements
     * @returns {string}
     */
    private setMultipleParams(elements: any) {

        let params = "";
        for (let element of elements) {
            if (element !== "undefined") {
                const orOp = (elements[elements.length - 1] === element) ? ")" : "%20OR%20";
                params += element + orOp;
            }
        }

        return params;
    }

    /**
     * Search with the filters.
     *
     * @param {Filter} filter
     */
    searchWithFilters(filter: Filter = new Filter()) {
        this.companies = [];
        this.setParams(filter);
        this.loadDatas();
    }

    /**
     * Reset this var: apiParams.
     */
    private resetApiParams() {
        this.apiParams = "";
    }

    /**
     * Check if the parameter is the first param, if is return nothing, if is not, return and "AND" operator.
     *
     * @returns {string | string}
     */
    private paramStart() {
        const paramStart = (this.apiParams.substr(this.apiParams.length - 1) === ")") ? "%20AND%20" : "";
        return paramStart;
    }

    /**
     * Data rows calculator.
     *
     * @param scrollNum
     * @returns {number}
     */
    private dataRowsCalculator(scrollNum: any) {
        let rows = 0;
        rows = (scrollNum > 0) ? 10 * scrollNum : 15;
        return rows;
    }
}
